import { createStackNavigator, createAppContainer } from "react-navigation";
import QuizIndex from './QuizIndex';
import Quiz from './Quiz';
import { registerRootComponent } from 'expo';

const MainStack = createStackNavigator({
    QuizIndex: {
        screen: QuizIndex,
        navigationOptions: {
            headerTitle:"Quizzes"
        }
    },
    Quiz: {
        screen: Quiz,
        navigationOptions: ({ navigation }) => ({
            headerTitle: navigation.getParam("title"),
            headerTintColor: "#fff",
            headerStyle: {
                backgroundColor: navigation.getParam("color"),
                borderBottomColor: navigation.getParam("color")
            }
        })
    }
});

export default registerRootComponent(createAppContainer(MainStack));

// import Quiz from './Quiz';
// import { registerRootComponent } from 'expo';
// export default registerRootComponent(Quiz);